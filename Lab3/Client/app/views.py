from flask import render_template, request, redirect, url_for, json

from app import app
from .forms import BookForm, AuthorForm, LanguageForm, GenreForm
from config import url
import requests

@app.route('/')
def index():
    return render_template('navigation.html', title = 'Main')


@app.route('/books/')
def books():
    response = requests.get(url + '/books')
    books = json.loads(response.text)
    return render_template('books.html', title = 'Books', books=books)


@app.route('/books/add/', methods=['get', 'post'])
def add_book():
    form = BookForm()
    if form.validate_on_submit():
        book = update_book(form)
        requests.post(url + '/books', json = book)
        return redirect(url_for('books'))
    return render_template('add_edit.html', title='Add book', form=form)


@app.route("/books/edit/<int:book_id>/", methods=['get', 'post'])
def edit_book(book_id):
    form = BookForm()
    book = update_book(form)
    if form.validate_on_submit():
        requests.patch(url + '/books/{}'.format(book_id), json = book)
        return redirect(url_for('books'))
    else:
        form.title.data = book['title']
        form.author.data = book['author']
        form.genre.data = book['genre']
        form.lang.data = book['lang']
        form.summary.data = book['summary']
        return render_template('add_edit.html', title='Edit book', form=form)


@app.route('/books/delete/<int:book_id>/', methods=['get', 'post'])
def delete_book(book_id):
    book = json(requests.get(url + '/books/{}'.format(book_id)).text)
    if request.method == 'POST':
        requests.delete(url + '/books/{}'.format(book_id))
        return redirect(url_for('books'))
    else:
        return render_template('delete.html', title='Delete book', instance=book)


@app.route('/authors/')
def show_authors():
    response = requests.get(url + '/authors')
    authors = json.loads(response.text)
    return render_template('authors.html', title='Authors',  authors=authors)


@app.route('/authors/add/', methods=['get', 'post'])
def add_author():
    form = AuthorForm()
    #author = update_author(form)
    if form.validate_on_submit():
        author = update_author(form)
        requests.post(url + '/authors', json = author)
        return redirect(url_for('show_authors'))
    return render_template('add_edit.html', title='Add author', form=form)


@app.route('/authors/edit/<int:author_id>/', methods=['get', 'post'])
def edit_author(author_id):
    form = AuthorForm()
    author = update_author(form)
    if form.validate_on_submit():
        requests.patch(url + '/authors/{}'.format(author_id), json = author)
        return redirect(url_for('show_authors'))
    else:
        form.name.data = author['name']
        form.date_of_birth.data = author['dob']
        form.date_of_death.data = author['dod']
        return render_template('add_edit.html', title='Edit author', form=form)


@app.route('/authors/delete/<int:author_id>/', methods=['get', 'post'])
def delete_author(author_id):
    author = json.loads(requests.get(url + '/authors/{}'.format(author_id)).text)
    if request.method == 'POST':
        requests.delete(uri + '/authors/{}'.format(author_id))
        return redirect(url_for('show_authors'))
    else:
        return render_template('delete.html', title='Delete author', instance=author)


@app.route('/languages/')
def show_languages():
    response = requests.get(url + '/languages')
    languages = json.loads(response.text)
    return render_template('languages.html', title = 'Languages', languages=languages)


@app.route('/languages/add', methods=['get', 'post'])
def add_language():
    form = LanguageForm()
    if form.validate_on_submit():
        requests.post(url + '/languages', json = {'lang': form.lang.data})
        return redirect(url_for('show_languages'))
    return render_template('add_edit.html', title='Add language', form=form)


@app.route('/languages/edit/<int:lang_id>/', methods=['get', 'post'])
def edit_language(lang_id):
    form = LanguageForm()
    lang  = form.lang.data
    if form.validate_on_submit():
        requests.patch(url + '/languages/{}'.format(lang_id), json = {'lang': lang})
        return redirect(url_for('show_languages'))
    else:
        form.lang.data = lang
        return render_template('add_edit.html', title='Edit language', form=form)


@app.route('/languages/delete/<int:lang_id>/', methods=['get', 'post'])
def delete_language(lang_id):
    print(url + '/languages/{}'.format(lang_id))
    lang = json.loads(requests.get(url + '/languages/{}'.format(lang_id)).text)
    if request.method == 'POST':
        requests.delete(url + '/languages/{}'.format(lang_id))
        return redirect(url_for('show_languages'))
    else:
        return render_template('delete.html', title='Delete language', instance=lang)


@app.route('/genres/')
def show_genres():
    genres = json.loads(requests.get(url + '/genres').text)
    return render_template('genres.html', title='Genres',  genres=genres)

@app.route('/genres/add', methods=['get', 'post'])
def add_genre():
    form = GenreForm()
    if form.validate_on_submit():
        requests.post(url + '/genres', json = {'genre': form.genre.data})
        return redirect(url_for('show_genres'))
    return render_template('add_edit.html', title='Add genre', form=form)


@app.route('/genres/edit/<int:genre_id>/', methods=['get', 'post'])
def edit_genre(genre_id):
    form = GenreForm()
    genre = form.genre.data
    if form.validate_on_submit():
        requests.patch(url + '/genres/{}'.format(genre_id), json = {'genre': genre})
        return redirect(url_for('show_languages'))
    else:
        form.genre.data = genre
        return render_template('add_edit.html', title='Edit genre', form=form)


@app.route('/genres/delete/<int:genre_id>/', methods=['get', 'post'])
def delete_genre(genre_id):
    genre = json.loads(requests.get(url + '/genres/{}'.format(genre_id)).text)
    if request.method == 'POST':
        requests.delete(url + '/genres/{}'.format(genre_id))
        return redirect(url_for('show_genres'))
    else:
        return render_template('delete.html', title='Delete genre', instance=genre)


def update_book(form):
    return {
            'title': form.title.data,
            'author': form.author.data,
            'genre': form.genre.data if form.genre.data != '' else None,
            'lang': form.lang.data if form.lang.data != '' else None,
            'summary': form.summary.data if form.summary.data != '' else None
        }


def update_author(form):
    return {
        'name': form.name.data,
        'dob': form.date_of_birth.data if form.date_of_birth.data != '' else None,
        'dod': form.date_of_death.data if form.date_of_death.data != '' else None
    }