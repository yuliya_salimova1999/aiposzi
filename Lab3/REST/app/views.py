from flask import request, jsonify
from flask_restful import Resource
from app import app, api
from .models import Book, BookSchema, Author, AuthorSchema, Genre, GenreSchema, Language, LanguageSchema, db

book_schema = BookSchema()
author_schema = AuthorSchema()
genre_schema = GenreSchema()
lang_schema = LanguageSchema()

class BooksListResource(Resource):
    def get(self):
        books = Book.query.all()
        books_schema = BookSchema(many = True)
        return jsonify(books_schema.dump(books))
    
    def post(self):
        new_book = update_book()
        update_book(new_book)
        db.session.add(new_book)
        db.session.commit()
        return jsonify(book_schema.dump(new_book))

api.add_resource(BooksListResource, '/books')

class BookResource(Resource):
    def get(self, book_id):
        book = Book.query.get_or_404(book_id)
        return jsonify(book_schema.dump(book))
    
    def patch(self, book_id):
        book = Book.query.get_or_404(book_id)
        book = update_book(book)

        db.session.commit()
        return jsonify(book_schema.dump(book))

    def delete(self, book_id):
        book = Book.query.get_or_404(book_id)
        db.session.delete(book)
        db.session.commit()
        return '', 204

api.add_resource(BookResource, '/books/<int:book_id>')

class AuthorsListResource(Resource):
    def get(self):
        authors = Author.query.all()
        authors_schema = AuthorSchema(many = True)
        return jsonify(authors_schema.dump(authors))
    
    def post(self):
        new_author = Author(
            name = request.json['name'],
            date_of_birth = request.json['dob'],
            date_of_death = request.json['dod']
        )
        db.session.add(new_author)
        db.session.commit()
        return jsonify(author_schema.dump(new_author))

api.add_resource(AuthorsListResource, '/authors')

class AuthorResource(Resource):
    def get(self, author_id):
        author = Author.query.get_or_404(author_id)
        return jsonify(author_schema.dump(author))
    
    def patch(self, author_id):
        author = Author.query.get_or_404(author_id)
            
        if 'name' in request.json:
            author.name = request.json['name']

        if 'dob' in request.json:
            author.date_of_birth = request.json['dob']

        if 'dod' in request.json:
            author.date_of_death = request.json['dod']

        db.session.commit()
        return jsonify(author_schema.dump(author))

    def delete(self, author_id):
        author = Author.query.get_or_404(author_id)
        db.session.delete(author)
        db.session.commit()
        return '', 204

api.add_resource(AuthorResource, '/authors/<int:author_id>')

class GenresListResource(Resource):
    def get(self):
        genres = Genre.query.all()
        genres_schema = GenreSchema(many = True)
        return jsonify(genres_schema.dump(genres))
    
    def post(self):
        new_genre = Genre(genre = request.json['genre'])
        db.session.add(new_genre)
        db.session.commit()
        return jsonify(genre_schema.dump(new_genre))

api.add_resource(GenresListResource, '/genres')

class GenreResource(Resource):
    def get(self, genre_id):
        genre = Genre.query.get_or_404(genre_id)
        print(genre)
        return jsonify(genre_schema.dump(genre))
    
    def patch(self, genre_id):
        genre = Genre.query.get_or_404(genre_id)
            
        if 'genre' in request.json:
            genre.genre = request.json['genre']

        db.session.commit()
        return jsonify(genre_schema.dump(genre))

    def delete(self, genre_id):
        genre = Genre.query.get_or_404(genre_id)
        db.session.delete(genre)
        db.session.commit()
        return '', 204

api.add_resource(GenreResource, '/genres/<int:genre_id>')

class LangsListResource(Resource):
    def get(self):
        langs = Language.query.all()
        langs_schema = LanguageSchema(many = True)
        return jsonify(langs_schema.dump(langs))
    
    def post(self):
        new_lang = Language(lang = request.json['lang'])
        db.session.add(new_lang)
        db.session.commit()
        return jsonify(lang_schema.dump(new_lang))

api.add_resource(LangsListResource, '/languages')

class LangResource(Resource):
    def get(self, lang_id):
        lang = Language.query.get_or_404(lang_id)
        return jsonify(lang_schema.dump(lang))
    
    def patch(self, lang_id):
        lang = Language.query.get_or_404(lang_id)
            
        if 'lang' in request.json:
            lang.lang = request.json['lang']

        db.session.commit()
        return jsonify(lang_schema.dump(lang))

    def delete(self, lang_id):
        lang = Language.query.get_or_404(lang_id)
        db.session.delete(lang)
        db.session.commit()
        return '', 204

api.add_resource(LangResource, '/languages/<int:lang_id>')

def update_book(book=None):
    if book is None:
        book = Book()

    if 'title' in request.json:
            book.title = request.json['title']

    if 'summary' in request.json:
            summary = request.json['summary']

    if 'author' in request.json:
            author_name = request.json['author']
            query_res = db.session.query(Author).filter(Author.name == author_name).first()
            if query_res is None:
                author = Author(name = author_name)
            else:
                author = query_res
                book.author = author

    if 'genre' in request.json:
        genre_name = request.json['genre']
        query_res = db.session.query(Genre).filter(Genre.genre == genre_name).first()
        if query_res is None:
            genre = Genre(genre = genre_name)
        else:
            genre = query_res
            book.genre = genre

    if 'lang' in request.json:
        lang_name = request.json['lang']
        query_res = db.session.query(Language).filter(Language.lang == lang_name).first()
        if query_res is None:
            lang = Language(lang = lang_name)
        else:
            lang = query_res
            book.lang = lang

    return book